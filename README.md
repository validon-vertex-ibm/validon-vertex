# Validon Vertex #



### What is Validon Vertex? ###

**Validon Vertex** is a STEM education & research software platform that provides accessible online courses; 
data analytics; storage & backup; automatically curated AI/ML models; and AR/VR 3D model visualizations.


DEMO VIDEO LINK:

https://vimeo.com/645654690